## Install Environment 

First install Anaconda or Miniconda.

Create conda environment (the yml-file has to be in the same directory):
```
conda env create -f environment.yml
```

Activate conda environment:
```
conda activate env_adp
```

Install epftoolbox [link to the docs](https://epftoolbox.readthedocs.io/en/latest/index.html):
```
git clone https://github.com/jeslago/epftoolbox.git
cd epftoolbox
```

If git is not installed, download and unpack the repository from [here](https://github.com/jeslago/epftoolbox) (remeber that you need to change the directory to the downloaded epftoolbox folder):
```
pip install .

```
Check that the packages are installed correctly:
```
conda list
```