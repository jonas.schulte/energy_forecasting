import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xgboost as xgb
from sklearn.metrics import mean_absolute_error, mean_squared_error
from epftoolbox.evaluation import sMAPE
import matplotlib.dates as mdates

def plot_price_forecast_weeks(y_test, prediction, best_weeks, worst_weeks):

    error = np.abs(y_test - prediction).mean(axis=1)
    
    if best_weeks:
    
        fig, axs = plt.subplots(3, 1, figsize=(16, 10))

        best_week = []

        for i in range(3):
            start_day = error.resample('W-Mon').mean().sort_values().index[i]
            row_index = y_test.index.get_loc(start_day)
            date_range = pd.period_range(start_day, periods=24*7, freq='H')
            timestamp_range = date_range.to_timestamp()
            best_week.append((timestamp_range, row_index))

            data_week = []
            prediction_week = []

            for j in range(7):
                data_week.extend(y_test.to_numpy()[best_week[i][1]+j,:24])
                prediction_week.extend(prediction[best_week[i][1]+j,:24])

            week_number = best_week[i][0].isocalendar()['week'].iloc[0]

            axs[i].plot(best_week[i][0], data_week, label='Day-Ahead Price', linewidth=3, zorder=1)
            axs[i].scatter(best_week[i][0], prediction_week, label='Prediction', color='lightseagreen', zorder=2)
            axs[i].set_xlabel(f'Week {week_number}', fontsize=14)

        axs[0].set_title('The three weeks with the best forecast accuracy', fontsize=16)
        axs[1].set_ylabel('Day-Ahead Price [€/MWh]', fontsize=14)
        axs[2].legend(loc='lower right')

        plt.tight_layout()
        plt.show()

    if worst_weeks:

        fig, axs = plt.subplots(3, 1, figsize=(16, 10))

        worst_week = []

        for i,n in zip(range(3), [0,2,3]):
            start_day = error.resample('W-Mon').mean().sort_values(ascending=False).index[n]
            row_index = y_test.index.get_loc(start_day)
            date_range = pd.period_range(start_day, periods=24*7, freq='H')
            timestamp_range = date_range.to_timestamp()
            worst_week.append((timestamp_range, row_index))

            data_week = []
            prediction_week = []

            for j in range(7):
                data_week.extend(y_test.to_numpy()[worst_week[i][1]+j,:24])
                prediction_week.extend(prediction[worst_week[i][1]+j,:24])

            week_number = worst_week[i][0].isocalendar()['week'].iloc[0]

            axs[i].plot(worst_week[i][0], data_week, label='Day-Ahead Price', linewidth=3, zorder=1, color='orange')
            axs[i].scatter(worst_week[i][0], prediction_week, label='Prediction', color='lightseagreen', zorder=2)
            axs[i].set_xlabel(f'Week {week_number}', fontsize=14)

        axs[0].set_title('The three weeks with the worst forecast accuracy', fontsize=16)
        axs[1].set_ylabel('Day-Ahead Price [€/MWh]', fontsize=14)
        axs[2].legend(loc='lower right')

        plt.tight_layout()
        plt.show()

def model_evaluation(p_real, p_pred):
    if isinstance(p_real, (np.ndarray)) == False:
        mae = mean_absolute_error(p_real.to_numpy(), p_pred)
        rmse = mean_squared_error(p_real.to_numpy(), p_pred, squared=False)
        smape = sMAPE(p_real=p_real.to_numpy(), p_pred=p_pred) * 100

        print(f'MAE on test set: {mae:0.2f}')
        print(f'RMSE on test set: {rmse:0.2f}')
        print(f'sMAPE on test set: {smape:0.2f}')
    else:
        mae = mean_absolute_error(p_real, p_pred)
        rmse = mean_squared_error(p_real, p_pred, squared=False)
        smape = sMAPE(p_real, p_pred) * 100

        print(f'MAE on test set: {mae:0.2f}')
        print(f'RMSE on test set: {rmse:0.2f}')
        print(f'sMAPE on test set: {smape:0.2f}')

def find_best_feature_set(X, y, X_test, y_test, n_estimators):
    
    print('Determine best feature set...')
    
    feature_sets = {
        "2018-2022": [2018, 2019, 2020, 2021, 2022],
        "2019-2022": [2019, 2020, 2021, 2022],
        "2020-2022": [2020, 2021, 2022],
        "2021-2022": [2021, 2022]
    }

    best_score = float('inf')
    best_feature_set = None

    for feature_set_combination in feature_sets.values():
        X_train = X.loc[X.index.year.isin(feature_set_combination)]
        y_train = y.loc[y.index.year.isin(feature_set_combination)]

        model = xgb.XGBRegressor(base_score=0.5, booster='gbtree',
                                 n_estimators=n_estimators,
                                 objective='reg:squarederror',
                                 learning_rate=0.01)
        model.fit(X_train, y_train, eval_set=[(X_train, y_train)], verbose=0)

        prediction = model.predict(X_test)
        score = sMAPE(p_real=y_test.to_numpy(), p_pred=prediction) * 100

        if score < best_score:
            best_score = score
            best_feature_set = feature_set_combination

    return best_feature_set

def get_growth_rates(df):
    first_year = df.index.year.min()

    result_df = pd.DataFrame()

    for column in df.columns:
        yearly_mean = df[column].groupby(df.index.year).mean()
        normalized_mean = yearly_mean / df[column].loc[df.index.year == first_year].mean()
    
        result_df[column] = normalized_mean

    return result_df

def plot_forecasts(price_forecast, CO2_forecast, features):
    end_time = features.index[-1].strftime('%Y%m%d')

    date_range = pd.period_range(end_time, periods=48, freq='H')
    timestamp_range = [x.to_timestamp() for x in date_range]

    fig, ax1 = plt.subplots(figsize=(16, 4))

    ax1.plot(timestamp_range, price_forecast, linewidth=3, label='Price Forecast')
    ax1.set_xlabel('Date and Time')
    ax1.set_ylabel('Day-Ahead Price [€/MWh]')

    ax2 = ax1.twinx()
    ax2.plot(timestamp_range, CO2_forecast, linewidth=3, color='orange', label='CO2 Forecast')
    ax2.set_ylabel('CO2 Intensity [gCO2/kWh]')

    ax1.set_title('CO2 Intensity and Day-Ahead Price Forecast')

    locator = mdates.AutoDateLocator()
    formatter = mdates.ConciseDateFormatter(locator)

    ax1.xaxis.set_major_locator(locator)
    ax1.xaxis.set_major_formatter(formatter)
    ax1.xaxis.set_minor_locator(mdates.HourLocator(interval=1))

    ax1.legend(loc='lower left')
    ax2.legend(loc='lower right')

    plt.show()