import csv
import logging
import os
import warnings
from datetime import datetime

import pandas as pd

from eta_co2_data import co2_equivalent

# Ignore pandas future warning
warnings.simplefilter(action="ignore", category=FutureWarning)
logging.basicConfig(level=logging.INFO)


# Please specify the API key here or set it as an environment variable
API_TOKEN = 'ENTSOE_API_KEY'

# Define time interval as datetime values
START_TIME = datetime.strptime("2023-06-01T00:00:00", "%Y-%m-%dT%H:%M:%S")
END_TIME = datetime.strptime("2023-07-01T00:00:00", "%Y-%m-%dT%H:%M:%S")

EXPORT_FILE_XLSX = "./output.xlsx"
EXPORT_FILE_CSV = "./CO2_2023.csv"
FREQ = "1H"


def write_co2_to_csv(api_token: str, start_time: datetime, end_time: datetime, freq: str) -> pd.Series:
    """Read CO2 equivalents from entso-e and write them to a csv file"""
    datetime_range = pd.date_range(start=start_time, end=end_time, freq=freq)
    query_timestamps = list(zip(datetime_range[:-1], datetime_range[1:]))

    co2_eqs = pd.Series(index=pd.MultiIndex.from_tuples(query_timestamps, names=["Start", "End"]))
    for index, value in co2_eqs.items():
        try:
            co2_eq = co2_equivalent.calculate_aggregated_co2_equivalent(api_token, index[0], index[1])
            with open(EXPORT_FILE_CSV, "a") as f:
                writer = csv.writer(f)
                writer.writerow((index[0], index[1], co2_eq))
            logging.info(f"{index}: {co2_eq}")
            co2_eqs[index] = co2_eq
        except Exception as e:
            logging.info(e)
            return co2_eqs
    return co2_eqs


def main() -> None:
    assert API_TOKEN is not None, "Please specifiy an API token"
    write_co2_to_csv(API_TOKEN, START_TIME, END_TIME, FREQ)


if __name__ == "__main__":
    main()