import pandas as pd
from functools import reduce
from datetime import date, timedelta

def get_train_data(client):

    print('The data is loaded from enstoe...')
    print('This can take some minutes.')

    country_code = 'DE_AT_LU'
    start = pd.Timestamp('20160101-00', tz='Europe/Brussels')
    end = pd.Timestamp('20180930-23', tz='Europe/Brussels')

    price_old = client.query_day_ahead_prices(country_code, start=start, end=end)

    country_code = 'DE_LU'
    start = pd.Timestamp('20181001-00', tz='Europe/Brussels')
    end = pd.Timestamp('20230630-23', tz='Europe/Brussels')

    price_new = client.query_day_ahead_prices(country_code, start=start, end=end)

    price = pd.concat([price_old, price_new])
    price.name = 'Day-Ahead Price [€/MWh]'

    country_code = 'DE_AT_LU'
    start = pd.Timestamp('20160101-00', tz='Europe/Brussels')
    end = pd.Timestamp('20180930-2345', tz='Europe/Brussels')

    load_old = client.query_load_forecast(country_code, start=start,end=end)

    country_code = 'DE_LU'
    start = pd.Timestamp('20181001-00', tz='Europe/Brussels')
    end = pd.Timestamp('20230630-2345', tz='Europe/Brussels')

    load_new = client.query_load_forecast(country_code, start=start,end=end)

    load = pd.concat([load_old, load_new])

    load_hour = load.resample('H').sum()
    load_hour.columns = ['Forecasted Load [summed up MW]']

    country_code = 'DE_AT_LU'
    start = pd.Timestamp('20160101-00', tz='Europe/Brussels')
    end = pd.Timestamp('20180930-2345', tz='Europe/Brussels')

    gen_old = client.query_wind_and_solar_forecast(country_code, start=start,end=end, psr_type=None)

    country_code = 'DE_LU'
    start = pd.Timestamp('20181001-00', tz='Europe/Brussels')
    end = pd.Timestamp('20230630-2345', tz='Europe/Brussels')

    gen_new = client.query_wind_and_solar_forecast(country_code, start=start,end=end, psr_type=None)

    gen = pd.concat([gen_old, gen_new])
    gen_hour = gen.sum(axis=1).resample('H').sum()
    gen_hour.name = 'Forecasted Renewable Generation [summed up MW]'

    dfs = [price, load_hour, gen_hour]
    df = reduce(lambda  left,right: pd.merge(left,right,left_index=True, right_index=True, how='outer'), dfs)

    print('The data was loaded successfully!')

    return df

def create_features_and_targets(df):
    
    names = ['Price', 'Load', 'Re_Gen']
    features_list = []

    for column, name in zip(df.columns, names):
        pivot_table = pd.pivot_table(df, values=column, index=df.index.date, columns=df.index.hour)
        pivot_table = pivot_table.dropna()
        pivot_table = pivot_table.add_prefix(name + '_')
        features_list.append(pivot_table)

    for lag in [1,2,7]:
        df_shifted = pd.pivot_table(df, values='Day-Ahead Price [€/MWh]', index=df.index.date, columns=df.index.hour).shift(lag).dropna()
        df_shifted = df_shifted.add_prefix('Price_')
        df_shifted = df_shifted.add_suffix('_' + str(lag))
        features_list.append(df_shifted)

    for lag in [1,7]:
        df_shifted = pd.pivot_table(df, values='Forecasted Load [summed up MW]', index=df.index.date, columns=df.index.hour).shift(lag).dropna()
        df_shifted = df_shifted.add_prefix('Load_')
        df_shifted = df_shifted.add_suffix('_' + str(lag))
        features_list.append(df_shifted)

    for lag in [1,7]:
        df_shifted = pd.pivot_table(df, values='Forecasted Renewable Generation [summed up MW]', index=df.index.date, columns=df.index.hour).shift(lag).dropna()
        df_shifted = df_shifted.add_prefix('Re_Gen_')
        df_shifted = df_shifted.add_suffix('_' + str(lag))
        features_list.append(df_shifted)
    
    features = pd.concat(features_list, axis=1).dropna()
    features.index = pd.to_datetime(features.index)
    features['dayofweek'] = features.index.dayofweek

    target = pd.pivot_table(df, values='Day-Ahead Price [€/MWh]', index=df.index.date, columns=df.index.hour).shift(-1).dropna()
    target = target.add_prefix('Target_Price_')
    target.index = pd.to_datetime(target.index)
    target = target[target.index.isin(features.index)]
    features = features[features.index.isin(target.index)]

    return features, target

def create_features(df):

    names = ['Price', 'Load', 'Re_Gen']
    features_list = []

    for column, name in zip(df.columns, names):
        pivot_table = pd.pivot_table(df, values=column, index=df.index.date, columns=df.index.hour)
        pivot_table = pivot_table.dropna()
        pivot_table = pivot_table.add_prefix(name + '_')
        features_list.append(pivot_table)

    for lag in [1,2,7]:
        df_shifted = pd.pivot_table(df, values='Day-Ahead Price [€/MWh]', index=df.index.date, columns=df.index.hour).shift(lag).dropna()
        df_shifted = df_shifted.add_prefix('Price_')
        df_shifted = df_shifted.add_suffix('_' + str(lag))
        features_list.append(df_shifted)

    for lag in [1,7]:
        df_shifted = pd.pivot_table(df, values='Forecasted Load [summed up MW]', index=df.index.date, columns=df.index.hour).shift(lag).dropna()
        df_shifted = df_shifted.add_prefix('Load_')
        df_shifted = df_shifted.add_suffix('_' + str(lag))
        features_list.append(df_shifted)

    for lag in [1,7]:
        df_shifted = pd.pivot_table(df, values='Forecasted Renewable Generation [summed up MW]', index=df.index.date, columns=df.index.hour).shift(lag).dropna()
        df_shifted = df_shifted.add_prefix('Re_Gen_')
        df_shifted = df_shifted.add_suffix('_' + str(lag))
        features_list.append(df_shifted)
    
    features = pd.concat(features_list, axis=1).dropna()
    features.index = pd.to_datetime(features.index)
    features['dayofweek'] = features.index.dayofweek

    return features

def get_data(client, start, end):
    print('The data is loaded from enstoe...')

    country_code = 'DE_LU'

    price = client.query_day_ahead_prices(country_code, start=start, end=end)
    price.name = 'Day-Ahead Price [€/MWh]'

    load = client.query_load_forecast(country_code, start=start,end=end)
    load_hour = load.resample('H').sum()
    load_hour.columns = ['Forecasted Load [summed up MW]']

    gen = client.query_wind_and_solar_forecast(country_code, start=start,end=end, psr_type=None)
    gen_hour = gen.sum(axis=1).resample('H').sum()
    gen_hour.name = 'Forecasted Renewable Generation [summed up MW]'

    dfs = [price, load_hour, gen_hour]
    df = reduce(lambda  left,right: pd.merge(left,right,left_index=True, right_index=True, how='outer'), dfs)

    return df

def get_todays_data(client):
    today = date.today()

    start_time = (today - timedelta(days=6)).strftime('%Y%m%d')
    end_time = (today + timedelta(days=1)).strftime('%Y%m%d')

    start = pd.Timestamp(f'{start_time}-00', tz='Europe/Brussels')
    end = pd.Timestamp(f'{end_time}-2345', tz='Europe/Brussels')

    df = get_data(client, start=start, end=end)

    all_data_available = True

    for column in df.columns:
        if df[column].isnull().any():
            print(f'{column} is not yet available!')
            all_data_available = False

    if df.index[-1].strftime('%Y%m%d') != end_time:
        all_data_available = False
        print('The data is not yet available!')

    if all_data_available:
        print('The data is complete.')
    else:
        today = date.today() - timedelta(days=1)
        start_time = (today - timedelta(days=6)).strftime('%Y%m%d')
        end_time = (today + timedelta(days=1)).strftime('%Y%m%d')
        start = pd.Timestamp(f'{start_time}-00', tz='Europe/Brussels')
        end = pd.Timestamp(f'{end_time}-2345', tz='Europe/Brussels')

        new_df = get_data(client, start=start, end=end)
        df = new_df
        print('Last day\'s data is loaded.')
    
    return df

def interpolate_zeros(df):
    indices_with_zero = df[df.eq(0).any(axis=1)].index

    for i in range(len(df[df.eq(0).any(axis=1)])):
        
        index_loc = df.index.get_loc(indices_with_zero[i])
        
        if 0 < index_loc < len(df) - 1:
            first_value = df.iloc[index_loc + 1, 0]
            second_value = df.iloc[index_loc - 1, 0]
            mean = (first_value + second_value) / 2
            df.iat[index_loc, 0] = mean

    return df

def create_co2_target(df, features):

    y_0 = pd.pivot_table(df, values='CO2', index=df.index.date, columns=df.index.hour).dropna()
    y_0 = y_0.add_prefix('CO2_')
    y_0 = y_0.add_suffix('_0')
    y_0.index = pd.to_datetime(y_0.index)

    y_1 = pd.pivot_table(df, values='CO2', index=df.index.date, columns=df.index.hour).shift(-1).dropna()
    y_1 = y_1.add_prefix('CO2_')
    y_1.index = pd.to_datetime(y_1.index)

    y = pd.merge(y_0, y_1, left_index=True, right_index=True)

    y = y[y.index.isin(features.index)]
    X = features[features.index.isin(y.index)]

    return y, X

def read_co2_csv(path):
    df = pd.read_csv(path, index_col=0)
    df.index = pd.to_datetime(df.index, utc=True)
    df.index = df.index.tz_convert('Europe/Brussels')
    df = df.drop(df.index[-2:])

    return df