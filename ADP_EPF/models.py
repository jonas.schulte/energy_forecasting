from sklearn.model_selection import TimeSeriesSplit, RandomizedSearchCV
from scipy.stats import randint, uniform
import xgboost as xgb
from sklearn.metrics import mean_absolute_error, mean_squared_error
from epftoolbox.evaluation import sMAPE
from keras.models import Sequential
from keras.layers import Dense, LSTM
from keras.callbacks import EarlyStopping
from keras.optimizers import Adam
import numpy as np
import time
import os

def get_best_hyperparameter_(X_train, y_train):
    tscv = TimeSeriesSplit(n_splits=5)
    xgb_model = xgb.XGBRegressor(base_score=0.5, booster='gbtree',
                                objective='reg:squarederror',
                                learning_rate=0.01)

    # Define the hyperparameter grid for RandomizedSearchCV
    param_grid = {
    'n_estimators': randint(low=500, high=2000),
    'max_depth': randint(low=1, high=15),
    }

    # Perform RandomizedSearchCV
    random_search = RandomizedSearchCV(
        estimator=xgb_model,
        param_distributions=param_grid,
        scoring='neg_mean_squared_error',
        n_iter=20,
        cv=tscv,
        verbose=1,
        n_jobs = -1
    )   

    # Fit the RandomizedSearchCV object to the training data
    random_search.fit(X_train, y_train)

    # Get the best estimator and evaluate on the test set
    best_model = random_search.best_estimator_
    
    # Create the "best_models" directory if it doesn't exist
    os.makedirs("best_models", exist_ok=True)

    # save model with unique name
    timestamp = time.strftime("%Y%m%d%H%M%S")
    filename = f'best_models/xgb_model_{timestamp}.txt'
    best_model.save_model(filename)

    return best_model

def fit_xgb_model(X_train, y_train, hyperparameter):
    if hyperparameter == True:
        return get_best_hyperparameter_(X_train, y_train)
    else:
        model = xgb.XGBRegressor(base_score=0.5, booster='gbtree',
                                n_estimators=500,
                                objective='reg:squarederror',
                                learning_rate=0.01)
        model.fit(X_train, y_train, eval_set=[(X_train, y_train)], verbose=100)

        return model

def fit_lstm_model(X_train, y_train):
    model = Sequential()

    model.add(LSTM(32, activation='relu', input_shape=(X_train.shape[1], X_train.shape[2])))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(y_train.shape[1]))

    monitor = EarlyStopping(monitor='val_loss', min_delta=1e-4, patience=5, 
                       verbose=1, mode='auto', restore_best_weights=True)

    model.compile(optimizer=Adam(learning_rate=0.001), loss='mae')
    model.summary()
    model.fit(X_train, y_train, epochs=100, validation_split=0.2, verbose=1, callbacks=[monitor])
    
    return model