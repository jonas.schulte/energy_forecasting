import numpy as np

def min_max_scaler(X, min_val, max_val):
    # Scale the data by subtracting the minimum value and dividing by the range (max - min)
    X_scaled = (X - min_val) / (max_val - min_val)
    return X_scaled

def inverse_min_max_scaler(X_scaled, min_val, max_val):
    # Invert the scaling for each column separately
    X_unscaled = np.zeros(X_scaled.shape)
    for i in range(X_scaled.shape[1]):
        X_unscaled[:, i] = X_scaled[:, i] * (max_val[0] - min_val[0]) + min_val[0]
    return X_unscaled

def WindowGenerator(dataset, n_future, n_past):
    #Empty lists to be populated using formatted training data
    X = []
    Y = []

    #Reformat input data into a shape: (n_samples x timesteps x n_features)
    for i in range(n_past, len(dataset) - n_future +1):
        X.append(dataset[i - n_past:i, :])
        Y.append(dataset[i:i + n_future, 0])
    
    X, Y = np.array(X), np.array(Y)

    print('X shape == {}.'.format(X.shape))
    print('Y shape == {}.'.format(Y.shape))

    return X, Y